from django import forms
from django.contrib.auth.models import User

class WpisForm(forms.Form):
    title = forms.CharField(max_length=1000)
    content=forms.CharField(max_length=200, min_length=10)

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'password')



from django.db import models


# Create your models here.
class Dane(models.Model):
    user = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    content=models.CharField(max_length=1000)
    published=models.DateTimeField()

    # On Python 3: def __str__(self):
    def __unicode__(self):
        return self.title

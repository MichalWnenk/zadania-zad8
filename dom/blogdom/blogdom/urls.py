from django.conf.urls import patterns, include, url
from django.views.generic import ListView


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),

    url(r'^login/$', 'fullblog.views.user_login', name='user_login'),

    url(r'^user/(?P<user>[\w\-]+)', 'fullblog.views.mojWpis', name='mojWpis'), #wsgi dodac zeby dzialalo

    url(r'^add$', 'fullblog.views.add', name='news'),

    url(r'^edit$', 'fullblog.views.editList', name='editList'),

    url(r'^editing/(?P<id>[\w\-]+)', 'fullblog.views.edit', name='edit'), #wsgi dodac zeby dzialalo

    #url(r'^blog/', 'fullblog.views.blog', name='blog'),
    url(r'^$', 'fullblog.views.blog', name='blog'),

#    url(r'^blog/user', 'fullblog.views.wpisUsera', name='user_wpis'),
    url(r'^logout/$','fullblog.views.user_logout', name='logout'),

    url(r'^register/$','fullblog.views.register', name='register'),
    url(r'^register/','fullblog.views.register', name='register'),

    )

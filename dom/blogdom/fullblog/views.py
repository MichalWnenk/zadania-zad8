# Create your views here.
# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect
from models import *
from form import *
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.formtools.preview import FormPreview
from datetime import datetime
from datetime import timedelta
from django.utils import timezone
from django.core.mail import send_mail
import random


def register(request):
        if request.method == 'GET':
            print request.META['PATH_INFO']
            if request.META['PATH_INFO'] != "/register/" and request.META['PATH_INFO'] != "/register" and request.META['PATH_INFO'] != "/" and request.META['PATH_INFO'] != "":
                activeUsers = ActiveUsers.objects.all().order_by('-pk')
                pathInfo = request.META['PATH_INFO']
                key = pathInfo.split('/')[2]

                for item in activeUsers:
                    if item.key == key:
                        return render_to_response('registerCheck.html')

                return HttpResponseRedirect('/')
            form = RegisterForm()
            c = RequestContext(request, locals())
            return render_to_response('register.html', c)

        if request.method == 'POST':
            form = RegisterForm(request.POST) # A form bound to the POST data
            if form.is_valid():

                 username = form.cleaned_data['username']
                 password = form.cleaned_data['password']
                 mail = form.cleaned_data['mail']
                 user = User.objects.create_user(username,mail,password)
                 user.save()
                 liczba1 = random.randint(10000000, 100000000)
                 liczba2 = random.randint(10000000, 100000000)
                 liczba3 = random.randint(10000000, 100000000)
                 liczba = str(liczba1) + str(liczba2) + str(liczba3)
                 send_mail('Mój Blog: Rejestracja użytkownika ' + str(username), 'Twoje konto zostało stworzone. Aby je aktywować kliknij w poniższy link:\r\n localhost:8000/register/' + liczba, 'odbiorcajavamail@gmail.com',[mail], fail_silently=False)
                 activeCheck = ActiveUsers(user=username, key=liczba)
                 activeCheck.save()
                 return render_to_response('base.html', {'typ': "ok"})

            form =RegisterForm()
            c = RequestContext(request, locals())
            return render_to_response('register.html', {'typ': "blad"}, c )

def blog(request):
    newsy= Dane.objects.all().order_by('-pk')
    users = User.objects.all()
    return render(request, 'base.html', {'newsy': newsy, 'users': users})


def add(request):
    if  request.user.is_authenticated():

        if request.method == 'GET':

            form = WpisForm(initial={'topic': u'Ta strona jest świetna!'})
            c = RequestContext(request, locals())
            return render_to_response('add.html', c)
        if request.method == 'POST':
            form = WpisForm(request.POST) # A form bound to the POST data
            if form.is_valid():

                nowy_wpis = form.save(commit=False)
                nowy_wpis.user = User.objects.get(id=request.user.id).username
                nowy_wpis.published = timezone.now()
                nowy_wpis.save()
                form.save_m2m()



                # title = form.data['title']
                # content = form.data['content']
                # user=User.objects.get(id=request.user.id).username
                # r = Dane(user=user, title=title, content=content, published = datetime.now())
                # r.save()
                #
                # tagsy = form.data['tags']
                # lista = tagsy.split(', ')
                #
                # for tag in tagsy:
                #     t =Tag(name=tag)
                #     t.save()
                #     tag.save()
                #     r.tags.add(tag)

                newsy= Dane.objects.all().order_by('-pk')
                users = User.objects.all()


            form =WpisForm(initial={'title': form.data['title'], 'content': form.data['content'], 'tags': form.data['tags']})
            c = RequestContext(request, locals())

            return render_to_response('add.html', {'typ': "blad"}, c )

    else:
        return redirect(reverse('blog'))

def user_login(request):

    if request.method == 'GET':
        form = LoginForm()
        c = RequestContext(request, locals())
        return render_to_response('login.html', c)

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():

            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(username=username, password=password)

            if user is not None:

                if user.is_active:
                    login(request, user)

                #  return HttpResponseRedirect('blog')
                    return redirect(reverse('blog'))
                else:
                    return HttpResponse("konto nieaktywne")
            else:
                print "Nie ma takiego użytkownika: {0}, {1}".format(login, password)
                return HttpResponse("Nie ma takiego użytkownika.")
        else:
            form = RegisterForm()
            c = RequestContext(request, locals())
            return HttpResponse("Login oraz hasło mogą mieć maksymalnie 100 znaków każdy")


# Use the login_required() decorator to ensure only those logged in can access the view.
@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return redirect(reverse('blog'))

def mojWpis(request, user):

    #user=User.objects.get(id=request.user.id)
    wpisy = Dane.objects.order_by('-pk')
    wpisyUsera=[]
    for wpis in wpisy:
        if wpis.user == user:
            wpisyUsera.append(wpis)

    return render_to_response('wpisyUsera.html',{'wpisy':wpisyUsera, 'user':user})

def editList(request):

    #zalogowany user moze zmieniac tylko swoje wpisy
    user=User.objects.get(id=request.user.id)
    wpisy = Dane.objects.order_by('-pk')
    wpisyEdytowane = []

    for wpis in wpisy:
        if wpis.user == user.username :
            if  (timezone.now() - wpis.published) < timedelta(minutes=10):
                wpisyEdytowane.append(wpis)

    if request.user.groups.filter(name="moderator"):
       wpisyEdytowane = Dane.objects.order_by('-pk')

    return render_to_response('editList.html', {'wpisy':wpisyEdytowane})

def edit(request, id):


    if  request.user.is_authenticated():


        wpisyAll = Dane.objects.all()
        instance=Dane()

        #moj wpis do edycji = wpisek
        for wpis in wpisyAll:
            if wpis.id==id:
                instance = wpis

        wpis = Dane.objects.get(id=id)

        time = timezone.now()

        if wpis.edited==True:
            time = wpis.editTime
        else:
            time = wpis.published



        if  (timezone.now() - time) < timedelta(minutes=10) and wpis.user == request.user.username or request.user.groups.filter(name="moderator"):
            if request.method == 'GET':


                form = WpisForm(instance=wpis)

                #c = RequestContext(request, locals())
                #return render_to_response('edit.html',{'id':id}, c)
                return render_to_response('edit.html', {'form':form, 'instance':instance}, context_instance=RequestContext(request))

            if request.method == 'POST':

                form = WpisForm(request.POST, instance=wpis) # A form bound to the POST data

                if form.is_valid():

                    # title = form.cleaned_data['tytul']
                    # content = form.cleaned_data['wpis']
                    # user=User.objects.get(id=request.user.id).username
                    # r = Dane(user=user, title=title, content=content, published = timezone.now())
                    # r.save()
                    #
                    # tagsy = form.data['tagi']
                    # lista = tagsy.split(', ')
                    #
                    # for tag in lista:
                    #     t =Tag(name=tag)
                    #     t.save()
                    #     r.tags.add(t)

                    edytowany_wpis = form.save(commit=False)
                    edytowany_wpis.editor = User.objects.get(id=request.user.id).username
                    edytowany_wpis.editTime=datetime.now()
                    edytowany_wpis.edited=True
                    edytowany_wpis.save()
                    form.save_m2m()



                    return redirect(reverse('blog'))

                form =WpisForm(instance=instance)
                c = RequestContext(request, locals())

                return redirect(reverse('blog'))
                #return render_to_response('edit.html', {'typ': "blad", 'tytul': tytul}, c )

    else:
        return redirect(reverse('blog'))

    return redirect(reverse('blog'))
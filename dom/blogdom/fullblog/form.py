# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from models import Dane


class WpisForm(forms.ModelForm):
   # tytul = forms.CharField(max_length=1000)
    #wpis =forms.CharField(max_length=200, min_length=10)
    #tagi = forms.CharField()

    class Meta:
        model = Dane
        exclude =['user','published','editor', 'edited','editTime', ]


    # def clean_tags(self):
    #     tags = self.cleaned_data['tags']
    #     return tags


# class UserForm(forms.ModelForm):
#     password = forms.CharField(widget=forms.PasswordInput())
#
#     class Meta:
#         model = User
#         fields = ('username', 'password')

class LoginForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(widget=forms.PasswordInput())

class RegisterForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(widget=forms.PasswordInput())
    mail = forms.EmailField(max_length=100)
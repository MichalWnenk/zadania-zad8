from django.contrib import admin
from models import *


class TagAdmin(admin.ModelAdmin):
    search_fields = ['name',]



class DaneAdmin(admin.ModelAdmin):
    list_display = ('user', 'published', 'title', )
    list_filter = ('published','tags', )
    search_fields = ('tags__name',)
    ordering = ('published',)




admin.site.register(Dane, DaneAdmin)
admin.site.register(Tag, TagAdmin)
from django.db import models


# Create your models here.

class ActiveUsers(models.Model):
    user = models.CharField(max_length=50)
    key = models.CharField(max_length=1000)

class Tag(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name
    class Meta:
        ordering = ('name',)

class Dane(models.Model):
    user = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    content=models.CharField(max_length=1000)
    published=models.DateTimeField()
    tags = models.ManyToManyField(Tag, blank=True)


    editor = models.CharField(max_length=255, default='')
    editTime = models.DateTimeField(blank=True, null=True)
    edited = models.BooleanField(default = False)

    # On Python 3: def __str__(self):
    def __unicode__(self):
        return self.title
    class Meta:


        ordering = ('title',)
